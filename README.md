# Cat Tele Feedback

Бот обратной связи для Telegram.

## Установка

### Безопасность

Если в стране, в которой запускается бот, существует опасность для его владельца - можно попросить кого-нибудь запустить бота из-за границы. В противном случае настоятельно рекомендуется следовать правилам, описанным в этом пункте.

Все манипуляции с интернетом надо проводить при включенном VPN на уровне всей системы (т.е. Opera VPN - не подойдёт). Инструкция по установке и использованию VPN не является частью этого документа.

Инструкция по установке написана для запуска на [Heroku](https://www.heroku.com). Если у вас есть собственное облачное решение - можете смело адаптировать её под себя. В качестве альтернативы можно использовать, например, [Oracle Cloud](https://www.oracle.com), [Amazon Web Services](https://aws.amazon.com), [Google Cloud](https://cloud.google.com).

В качестве сервиса для выполнения запроса по таймеру в инструкции используется [https://cron-job.org](https://cron-job.org). Вы можете поменять его на любую из существующих альтернатив, например, [EasyCron](https://www.easycron.com), [WebGazer](https://www.webgazer.io), [Healthchecks](https://healthchecks.io).

Использование альтернативных сервисов повышает безопасность.

Система, из которой устанавливается бот, должна быть зашифрована. Инструкция по шифрованию жёстких дисков не является частью этого документа.

В качестве дополнительной меры безопасности, можно проводить все манипуляции с виртуальной машины. Важно при этом, чтобы все правила безопасности распространялись и на гостевую систему тоже.

### Инструкция для программистов

Бот написан на **java 11**, сборка - на **gradle**, в качестве фреймворка используется **Spring Boot**. Данные хранятся в **PostgreSQL**.

Конфиг:

```
src/main/resources/application.yaml
```

Сборка:

```bash
./gradlew build
```

Запуск:

```bash
java -jar ./build/libs/feedback-app-1.0.jar
```

Переменные окружения:

```
FEEDBACKMASTERBOT_ID=<telegram bot id>
FEEDBACKMASTERBOT_KEY=<AES-256 encryption key>
JDBC_DATABASE_URL=jdbc:postgresql://<host>:<port>/<database>?<params>
```

### Пошаговая инструкция

0. Включите VPN.
1. Скачайте себе исходники бота, включая эту инструкцию.
2. Установите [git](https://git-scm.com/downloads).
3. Заведите анонимный e-mail ящик.
4. Зарегистрируйтесь в [Heroku](https://www.heroku.com), используя новый e-mail. При регистрации выберите язык `Java`.
5. Подключите к сервису кредитную карту, желательно виртуальную. Для этого перейдите в [Account Settings -> Billing](https://dashboard.heroku.com/account/billing) и заполните информацию. В отличиие от многих других сервисов, **Heroku** не проверяет карту списанием средств. **Тратить деньги не придётся.** Для чего это делается: бесплатный аккаунт имеет ряд ограничений, в частности по количеству времени в месяц, которое программа может бежать без остановки. Если карту не подключить - часть времени бот будет лежать. Подключение карты (без каких либо трат) увеличивает этот лимит до достаточного, чтобы бот бежал 24/7.
6. Установите приложение от **Heroku**: [heroku-cli](https://devcenter.heroku.com/articles/heroku-cli#download-and-install).
7. Создайте в телеграмме бота, используя [BotFather](https://t.me/botfather). Сохраните идентификатор бота, он вам пригодится.
8. Получите ключ для шифрования данных. Все данные, которые хранятся в базе данных, будут зашифрованы этим ключом. Для получения ключа, откройте в браузере страницу `tools/aes256.html`. Длинный набор цифр и букв, который вы увидите, и есть нужный ключ. Обратите внимание, каждый раз при обновлении страницы генерируется новый ключ. Таким образом, ваш будет абсолютно уникальным. Сохрание его.
9. Создайте в **Heroku** приложение. Для этого перейдите в [Apps](https://dashboard.heroku.com/apps), нажмите на [Create new app](https://dashboard.heroku.com/new-app), задайте имя (используйте английские буквы, цифры и знак `"-"` и только их) и нажмите `Create app`. Запомните заданное имя.
10. Задайте переменные окружения. Для этого перейдите на вкладку `Settings` и нажмите на `Reveal config vars`. В появившейся форме надо будет задать две пары `KEY/VALUE`, после каждой нажимая на кнопку `Add`. Первый ключ (`KEY`): `FEEDBACKMASTERBOT_ID`, значение (`VALUE`): идентификатор телеграмм-бота, сохранённый в пункте 7. Второй ключ: `FEEDBACKMASTERBOT_KEY`, значение - ключ шифрования, сохранённый в пункте 8.

На данном этапе все подготовительные работы сделаны и можно приступать к разворачиванию и запуску приложения.

11. Создайте на вашем компьютере папку, в которой вы будете хранить исходные коды бота. Откройте командную строку в этой папке.
12. Залогиньтесь в приложении, выполнив команду `heroku login` и пройдя всю процедуру.
13. Выполните по очереди следующие команды (`<имя приложения>` - имя, заданное в пункте 9):

```bash
git init
heroku git:remote -a <имя приложения>
git add .
git commit -am "Init."
```

В результате этих действий в созданной папке должна появиться папка `.git`.

14. Скопируйте в созданную папку исходники бота, сохранённые в пункте 1. Важно при этом соблюдать структуру папок. Если вы качали архив - предварительно его распакуйте. Если в исходниках бота есть папка с именем `.git` - её копировать не надо! В результате в созданной папке рядом с `.git` должны появиться папки `gradle`, `project`, `src`, `tools` и файлы `build.gradle`, `gradle.properties`, `gradlew`, `gradlew.bat`, `LICENSE`, `Procfile`, `README.md`, `settings.gradle`, `system.properties`.
15. Выполните по очереди следующие команды:

```bash
git add -A
git commit -m "Versin 1.0"
git push heroku master
```

Если вы всё сделали правильно, после выполнения последней команды должна начаться сборка проекта, которая завершится запуском бота. После того, как команда отработает, проверьте корректность работы приложения. Для этого откройте страницу `https://<имя приложения>.herokuapp.com/api/v1/test`, где `<имя приложения>` - имя, заданное в пункте 9 данной инструкции. Если всё прошло успешно, должна появиться надпись `Application is running!`. Если надпить появилась - откройте в телеграмме своего бота и напишите команду `/help`. Если бот напечатал помощь, значит вы всё сделали правильно.

16. Вылогиньтесь из приложения, выполнив команду `heroku logout`.

Остался последний шаг. Ради экономии ресурсов, **Heroku** останавливает приложения после некоторого промежутка неактивности. К сожалению, такой подход плохо сочетается с работой телеграмм-ботов, поскольку вынуждает постоянно запускать бота вручную. К счастью, существует простой способ добиться того, чтобы приложение не останавливалось. Для этого надо сделать следующее:

17. Зайдите на сайт [https://cron-job.org](https://cron-job.org), зарегистрируйтесь в нём, используя новую почту (адрес необходимо подтвердить), [залогиньтесь](https://cron-job.org/en/members/), перейдите на [список задач](https://cron-job.org/en/members/jobs/), нажмите на [create cronjob](https://cron-job.org/en/members/jobs/add/), задайте название (`Title`), в качестве адреса пропишите `https://<имя приложения>.herokuapp.com/api/v1/test`, где `<имя приложения>` - имя, заданное в пункте 9, и нажмите на кнопку `Create cronjob` внизу страницы.

Теперь, каждые 15 минут будет посылаться запрос, который не даст **Heroku** остановить бота автоматически.
