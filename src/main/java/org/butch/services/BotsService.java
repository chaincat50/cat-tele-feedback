package org.butch.services;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.butch.bots.base.Bot;
import org.butch.bots.feedback.master.FeedbackMasterBot;
import org.butch.encryption.CryptoUtils;
import org.butch.repositories.FeedbackStorageImpl;
import org.butch.repositories.VersionStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings("unused")
@Service
public class BotsService {
    private final Logger LOGGER = LoggerFactory.getLogger(BotsService.class);

    private final List<BotRunnable> bots = new ArrayList<>();

    // Don't use those directly
    private DataSource pDataSource;
    private VersionStorage pVersionStorage;

    @Scheduled(fixedRate = 1000L)
    public void timer() {
        final long now = System.currentTimeMillis();

        for (BotRunnable runnable : bots) {
            try {
                runnable.bot.onTimer(now);
                if (runnable.ex != null) {
                    LOGGER.info(runnable.bot.getBotUsername() + " recovered");
                    runnable.ex = null;
                }
            } catch (Throwable ex) {
                if (runnable.ex == null) {
                    LOGGER.error(runnable.bot.getBotUsername() + " exception", ex);
                }
                runnable.ex = ex;
            }
        }
    }

    @PostConstruct
    public void init() throws TelegramApiException {
        final String dbUrl = System.getenv("JDBC_DATABASE_URL");

        final TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);

        tryInitBot("feedback", botsApi,
                () -> {
                    final String token = System.getenv("FEEDBACKMASTERBOT_ID");
                    final String key = System.getenv("FEEDBACKMASTERBOT_KEY");
                    final DataSource dataSource = getDataSource(dbUrl);
                    final VersionStorage versionStorage = getVersionStorage(dataSource);
                    final FeedbackStorageImpl storage = new FeedbackStorageImpl(dataSource, versionStorage);
                    return new FeedbackMasterBot(botsApi, token, storage, CryptoUtils.keyFromString(key));
                }
        );
    }

    private void tryInitBot(String name, TelegramBotsApi botsApi, Callable<Bot> producer) {
        LOGGER.info("Starting " + name + " bot.");
        try {
            initBot(botsApi, producer.call());
        } catch (Throwable ex) {
            LOGGER.error("Could not init bot " + name, ex);
        }
    }

    private void initBot(TelegramBotsApi botsApi, Bot bot) throws TelegramApiException {
        bots.add(new BotRunnable(bot));
        botsApi.registerBot(bot);
    }

    private DataSource getDataSource(String dbUrl) {
        if (pDataSource == null) {
            final HikariConfig config = new HikariConfig();
            config.setJdbcUrl(dbUrl);
            pDataSource = new HikariDataSource(config);
        }
        return pDataSource;
    }

    private VersionStorage getVersionStorage(DataSource dataSource) {
        if (pVersionStorage == null) {
            pVersionStorage = new VersionStorage(dataSource);
            pVersionStorage.init();
        }
        return pVersionStorage;
    }

    private static class BotRunnable {
        private final Bot bot;
        private Throwable ex = null;

        private BotRunnable(Bot bot) {
            this.bot = bot;
        }
    }
}
