package org.butch.repositories;

import org.butch.bots.feedback.master.repository.CommonDao;
import org.butch.bots.feedback.master.repository.Storage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

public class FeedbackStorageImpl implements Storage {
    private static final String NAME = "feedback_bot";
    private static final String[] VERSIONS = {
            null,
            "1",
            "2",
            "3",
            "4"
    };
    private static final HashMap<String, InitFunction> INIT_FUNCTIONS = new HashMap<>();

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final DataSource dataSource;
    private final VersionStorage versionStorage;

    static {
        INIT_FUNCTIONS.put(VERSIONS[0], FeedbackStorageImpl::initV0ToV1);
        INIT_FUNCTIONS.put(VERSIONS[1], FeedbackStorageImpl::initV1ToV2);
        INIT_FUNCTIONS.put(VERSIONS[2], FeedbackStorageImpl::initV2ToV3);
        INIT_FUNCTIONS.put(VERSIONS[3], FeedbackStorageImpl::initV3ToV4);
    }

    public FeedbackStorageImpl(DataSource dataSource, VersionStorage versionStorage) {
        this.dataSource = dataSource;
        this.versionStorage = versionStorage;
    }

    @Override
    public void init() {
        String version = versionStorage.getVersion(NAME);
        try (Connection connection = dataSource.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                for (int i = 0; i < VERSIONS.length - 1; ++i) {
                    if (Objects.equals(version, VERSIONS[i])) {
                        final InitFunction initFunction = INIT_FUNCTIONS.get(version);
                        version = VERSIONS[i + 1];
                        LOGGER.info("Updating " + NAME + " schema to version " + version);
                        initFunction.init(stmt);
                    }
                }
            }
        } catch (SQLException ex) {
            LOGGER.error("init", ex);
        }
        if (!versionStorage.setVersion(NAME, VERSIONS[VERSIONS.length - 1])) {
            LOGGER.error("Could not update version: " + NAME);
        }
    }

    private static void initV0ToV1(Statement stmt) throws SQLException {
        stmt.executeUpdate("DROP TABLE IF EXISTS feedback_bots;");
        stmt.executeUpdate("" +
                "CREATE TABLE IF NOT EXISTS feedback_bots ( " +
                "id SERIAL PRIMARY KEY, " +
                "body TEXT" +
                ");"
        );
    }

    private static void initV1ToV2(Statement stmt) throws SQLException {
        stmt.executeUpdate("DROP TABLE IF EXISTS feedback_chat_ids;");
        stmt.executeUpdate("" +
                "CREATE TABLE IF NOT EXISTS feedback_chat_ids ( " +
                "key VARCHAR(256) PRIMARY KEY, " +
                "chat_id VARCHAR(256)" +
                ");"
        );
    }

    private static void initV2ToV3(Statement stmt) throws SQLException {
        stmt.executeUpdate("" +
                "ALTER TABLE feedback_chat_ids " +
                "ADD COLUMN IF NOT EXISTS " +
                "create_time TIMESTAMP;"
        );
        stmt.executeUpdate("" +
                "UPDATE feedback_chat_ids SET create_time = now();"
        );
    }

    private static void initV3ToV4(Statement stmt) throws SQLException {
        stmt.executeUpdate("DROP TABLE IF EXISTS feedback_bans;");
        stmt.executeUpdate("" +
                "CREATE TABLE IF NOT EXISTS feedback_bans ( " +
                "id SERIAL PRIMARY KEY, " +
                "body TEXT" +
                ");"
        );
    }

    @Override
    public int insertBot(CommonDao dao) {
        LOGGER.debug("insertBot: " + dao);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "INSERT INTO feedback_bots (body) " +
                    "VALUES (?) " +
                    "RETURNING id;")) {
                stmt.setString(1, dao.getBody());
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                LOGGER.error("id was not returned");
                return -1;
            }
        } catch (Exception ex) {
            LOGGER.error("insertBot: " + dao, ex);
            return -1;
        }
    }

    @Override
    public boolean updateBot(CommonDao dao) {
        LOGGER.debug("updateBot: " + dao);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "UPDATE feedback_bots SET " +
                    "body = ? " +
                    "WHERE id = ?;")) {
                stmt.setString(1, dao.getBody());
                stmt.setInt(2, dao.getId());
                return stmt.executeUpdate() == 1;
            }
        } catch (Exception ex) {
            LOGGER.error("updateBot: " + dao, ex);
            return false;
        }
    }

    @Override
    public boolean deleteBot(CommonDao dao) {
        LOGGER.debug("deleteBot: " + dao);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "DELETE FROM feedback_bots WHERE id = ?;")) {
                stmt.setInt(1, dao.getId());
                return stmt.executeUpdate() == 1;
            }
        } catch (Exception ex) {
            LOGGER.error("deleteBot: " + dao, ex);
            return false;
        }
    }

    @Override
    public List<CommonDao> queryBots() {
        LOGGER.debug("queryBots");
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "SELECT id, body FROM feedback_bots ORDER BY id;")) {
                final ResultSet resultSet = stmt.executeQuery();
                final List<CommonDao> ret = new ArrayList<>();
                while (resultSet.next()) {
                    final CommonDao dao = new CommonDao();
                    dao.setId(resultSet.getInt(1));
                    dao.setBody(resultSet.getString(2));
                    ret.add(dao);
                }
                return ret;
            }
        } catch (Exception ex) {
            LOGGER.error("queryBots", ex);
            return Collections.emptyList();
        }
    }

    @Override
    public boolean insertChatId(String key, String chatId) {
        LOGGER.debug("insertChatId: " + key + ", " + chatId);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "INSERT INTO feedback_chat_ids (key, chat_id, create_time) " +
                    "VALUES (?, ?, now());")) {
                stmt.setString(1, key);
                stmt.setString(2, chatId);
                return stmt.executeUpdate() == 1;
            }
        } catch (Exception ex) {
            LOGGER.error("insertChatId: " + key + ", " + chatId, ex);
            return false;
        }
    }

    @Override
    public String getChatId(String key) {
        LOGGER.debug("getChatId: " + key);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "SELECT chat_id FROM feedback_chat_ids WHERE key = ?;")) {
                stmt.setString(1, key);
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getString(1);
                }
                return null;
            }
        } catch (Exception ex) {
            LOGGER.error("getChatId: " + key, ex);
            return null;
        }
    }

    @Override
    public void deleteOldChatIds() {
        LOGGER.debug("deleteOldChatIds");
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "DELETE FROM feedback_chat_ids " +
                    "WHERE create_time < now() - interval '168 hours';")) {
                int count = stmt.executeUpdate();
                if (count > 0) {
                    LOGGER.info("Deleted old chat ids: " + count);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("deleteOldChatIds", ex);
        }
    }

    @Override
    public int insertBan(CommonDao dao) {
        LOGGER.debug("insertBan: " + dao);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "INSERT INTO feedback_bans (body) " +
                    "VALUES (?) " +
                    "RETURNING id;")) {
                stmt.setString(1, dao.getBody());
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                LOGGER.error("id was not returned");
                return -1;
            }
        } catch (Exception ex) {
            LOGGER.error("insertBan: " + dao, ex);
            return -1;
        }
    }

    @Override
    public boolean deleteBan(CommonDao dao) {
        LOGGER.debug("deleteBan: " + dao);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "DELETE FROM feedback_bans WHERE id = ?;")) {
                stmt.setInt(1, dao.getId());
                return stmt.executeUpdate() == 1;
            }
        } catch (Exception ex) {
            LOGGER.error("deleteBan: " + dao, ex);
            return false;
        }
    }

    @Override
    public List<CommonDao> queryBans() {
        LOGGER.debug("queryBans");
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "SELECT id, body FROM feedback_bans;")) {
                final ResultSet resultSet = stmt.executeQuery();
                final List<CommonDao> ret = new ArrayList<>();
                while (resultSet.next()) {
                    final CommonDao dao = new CommonDao();
                    dao.setId(resultSet.getInt(1));
                    dao.setBody(resultSet.getString(2));
                    ret.add(dao);
                }
                return ret;
            }
        } catch (Exception ex) {
            LOGGER.error("queryBans", ex);
            return Collections.emptyList();
        }
    }
}
