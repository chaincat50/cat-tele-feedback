package org.butch.repositories;

import java.sql.SQLException;
import java.sql.Statement;

public interface InitFunction {
    void init(Statement stmt) throws SQLException;
}
