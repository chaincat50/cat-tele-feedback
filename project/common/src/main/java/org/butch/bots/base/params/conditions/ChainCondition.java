package org.butch.bots.base.params.conditions;

import org.butch.bots.base.params.Result;
import org.butch.bots.base.params.terms.ListTerm;
import org.butch.bots.base.params.terms.Term;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChainCondition extends Condition {
    private final List<Condition> conditions;
    private final List<Term> terms = new ArrayList<>();
    private Result result = Result.PARTIAL_MATCH;

    public ChainCondition(Condition... conditions) {
        this.conditions = Arrays.asList(conditions);
    }

    public static ChainCondition create(Condition... conditions) {
        return new ChainCondition(conditions);
    }

    @Override
    public Result accept(char c) throws IllegalStateException {
        if (result == Result.NO_MATCH)
            return Result.NO_MATCH;
        if (result == Result.EXACT_MATCH)
            throw new IllegalStateException("chain: ExactMatch");
        if (conditions.isEmpty()) {
            result = Result.EXACT_MATCH;
            return Result.EXACT_MATCH;
        }
        if (terms.size() >= conditions.size())
            throw new IllegalStateException("chain: Index");
        final Condition current = conditions.get(terms.size());
        try {
            final Result r = current.accept(c);
            if (r == Result.NO_MATCH) {
                result = Result.NO_MATCH;
                return Result.NO_MATCH;
            }
            if (r == Result.EXACT_MATCH) {
                terms.add(current.getTerm());
                if (conditions.size() == terms.size()) {
                    result = Result.EXACT_MATCH;
                    return Result.EXACT_MATCH;
                }
            }
            return Result.PARTIAL_MATCH;
        } catch (IllegalStateException ex) {
            throw new IllegalStateException("chain: " + ex.getMessage());
        }
    }

    @Override
    public void reset() {
        for (Condition condition : conditions) {
            condition.reset();
        }
        terms.clear();
        result = Result.PARTIAL_MATCH;
    }

    @Override
    public ListTerm getTerm() throws IllegalStateException {
        if (result != Result.EXACT_MATCH)
            throw new IllegalStateException("chain: " + terms.size());
        return new ListTerm(terms);
    }

    public List<Term> getTerms() {
        return terms;
    }
}
