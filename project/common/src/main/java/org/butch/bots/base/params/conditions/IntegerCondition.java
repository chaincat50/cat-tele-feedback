package org.butch.bots.base.params.conditions;

import org.butch.bots.base.params.ParamUtils;
import org.butch.bots.base.params.Result;
import org.butch.bots.base.params.terms.IntegerTerm;

public class IntegerCondition extends Condition {
    private Long value = null;
    private Result result = Result.PARTIAL_MATCH;

    public IntegerCondition() {
    }

    public static IntegerCondition create() {
        return new IntegerCondition();
    }

    @Override
    public Result accept(char c) throws IllegalStateException {
        if (result == Result.NO_MATCH)
            return Result.NO_MATCH;
        if (result == Result.EXACT_MATCH)
            throw new IllegalStateException("integer: ExactMatch");
        if ('0' <= c && c <= '9') {
            if (value == null) {
                value = 0L;
            }
            value = value * 10 + (c - '0');
            return Result.PARTIAL_MATCH;
        }
        if (ParamUtils.isSeparator(c)) {
            if (value == null) {
                return Result.PARTIAL_MATCH;
            }
            result = Result.EXACT_MATCH;
            return Result.EXACT_MATCH;
        }
        result = Result.NO_MATCH;
        return Result.NO_MATCH;
    }

    @Override
    public void reset() {
        value = null;
        result = Result.PARTIAL_MATCH;
    }

    @Override
    public IntegerTerm getTerm() throws IllegalStateException {
        if (result != Result.EXACT_MATCH)
            throw new IllegalStateException("integer: " + value);
        return new IntegerTerm(value);
    }
}
