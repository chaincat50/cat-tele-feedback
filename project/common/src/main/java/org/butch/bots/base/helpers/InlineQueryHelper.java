package org.butch.bots.base.helpers;

import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.Collection;

public class InlineQueryHelper {
    private final TelegramLongPollingBot bot;

    public InlineQueryHelper(TelegramLongPollingBot bot) {
        this.bot = bot;
    }

    public AnswerBuilder answer(InlineQuery query) {
        return new AnswerBuilder(query.getId());
    }

    public boolean sendAnswer(AnswerInlineQuery msg) throws TelegramApiException {
        return bot.execute(msg);
    }

    public static class AnswerBuilder {
        private final AnswerInlineQuery result = new AnswerInlineQuery();

        public AnswerBuilder(String id) {
            result.setInlineQueryId(id);
            result.setResults(new ArrayList<>());
        }

        public AnswerBuilder offset(String offset) {
            result.setNextOffset(offset);
            return this;
        }

        public AnswerBuilder offset(int offset) {
            result.setNextOffset(Integer.toString(offset));
            return this;
        }

        public AnswerBuilder privateMode(@NotNull String text, @NotNull String parameter) {
            result.setSwitchPmText(text);
            result.setSwitchPmParameter(parameter);
            return this;
        }

        public AnswerBuilder add(@NotNull InlineQueryResult item) {
            result.getResults().add(item);
            return this;
        }

        public AnswerBuilder addAll(@NotNull Collection<InlineQueryResult> items) {
            result.getResults().addAll(items);
            return this;
        }

        public AnswerInlineQuery build() {
            return result;
        }
    }
}
