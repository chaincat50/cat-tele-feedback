package org.butch.bots.base.params2.conditions;

import org.butch.bots.base.params2.ParamUtils;

public class IntegerCondition implements Condition {
    private final boolean required;

    private String term = "";
    private boolean finished = false;
    private boolean error = false;

    private IntegerCondition(boolean required) {
        this.required = required;
    }

    public static IntegerCondition required() {
        return new IntegerCondition(true);
    }

    public static IntegerCondition optional() {
        return new IntegerCondition(false);
    }

    @Override
    public void reset() {
        term = "";
        finished = false;
        error = false;
    }

    @Override
    public boolean accept(char c) {
        if (ParamUtils.isSeparator(c)) {
            if (term.isEmpty())
                return true;
            finished = true;
            return false;
        }
        if (c < '0' || c > '9') {
            error = true;
            return false;
        }
        term += c;
        return true;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public boolean isComplete() {
        return finished && !error;
    }

    @Override
    public String getTerm() {
        return term;
    }
}
