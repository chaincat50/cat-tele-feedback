package org.butch.bots.base.helpers.consts;

public enum ChatMemberStatus {
    CREATOR("creator"),
    ADMINISTRATOR("administrator"),
    MEMBER("member"),
    RESTRICTED("restricted"),
    LEFT("left"),
    BANNED("kicked");

    private final String value;

    ChatMemberStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
