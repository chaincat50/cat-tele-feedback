package org.butch.bots.base.messages;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.inlinequery.ChosenInlineQuery;

public class InlineSelect extends Base {
    public InlineSelect(Update update) {
        super(update);
    }

    @Override
    public Type getType() {
        return Type.INLINE_SELECT;
    }

    public ChosenInlineQuery getQuery() {
        return getUpdate().getChosenInlineQuery();
    }
}
