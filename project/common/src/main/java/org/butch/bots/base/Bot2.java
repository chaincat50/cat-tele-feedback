package org.butch.bots.base;

import org.butch.bots.base.error.writer.ErrorWriter;
import org.butch.bots.base.error.writer.LogErrorWriter;
import org.butch.bots.base.filter.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

public abstract class Bot2 extends AbstractBot {
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final ArrayList<FilterHolder> filters = new ArrayList<>();
    private ErrorWriter defaultErrorWriter = new LogErrorWriter(LOGGER);

    private User me;

    public User getBotUser() {
        return me;
    }

    public void addFilter(@NotNull Filter filter) {
        filters.add(new FilterHolder(filter, null));
    }

    public void setDefaultErrorWriter(ErrorWriter defaultErrorWriter) {
        this.defaultErrorWriter = defaultErrorWriter;
    }

    public void addFilter(@NotNull Filter filter, @NotNull ErrorWriter errorWriter) {
        filters.add(new FilterHolder(filter, errorWriter));
    }

    @Override
    public void onRegister() {
        try {
            me = getMe();
        } catch (TelegramApiException ex) {
            LOGGER.error("Telegram exception: " + ex, ex);
        }
        for (FilterHolder filterHolder : filters) {
            final Filter filter = filterHolder.getFilter();
            try {
                LOGGER.info(filter.getName() + ": registering...");
                if (filter.register(this)) {
                    LOGGER.info(filter.getName() + ": registered successfully.");
                } else {
                    LOGGER.warn(filter.getName() + ": could not register.");
                }
            } catch (TelegramApiException ex) {
                LOGGER.warn(filter.getName() + ": registration error", ex);
            }
        }
    }

    @Override
    public void onUpdateReceived(@NotNull Update update) {
        boolean errorFail = false;
        for (FilterHolder filterHolder : filters) {
            final Filter filter = filterHolder.getFilter();
            final ErrorWriter errorWriter = filterHolder.getErrorWriter(defaultErrorWriter);
            try {
                filter.accept(update);
            } catch (TelegramApiException ex) {
                if (errorWriter != null && !errorWriter.accept(update, filter.getName(), ex)) {
                    if (!errorFail) {
                        errorFail = true;
                        LOGGER.warn(update.toString(), ex);
                    }
                }
            }
        }
    }

    private static class FilterHolder {
        private final Filter filter;
        private final ErrorWriter errorWriter;

        public FilterHolder(Filter filter, ErrorWriter errorWriter) {
            this.filter = filter;
            this.errorWriter = errorWriter;
        }

        public Filter getFilter() {
            return filter;
        }

        public ErrorWriter getErrorWriter(ErrorWriter defaultWriter) {
            if (errorWriter != null)
                return errorWriter;
            return defaultWriter;
        }
    }
}
