package org.butch.bots.base.commands;

import org.butch.bots.base.messages.CommandMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public abstract class Command<TBot> {
    public static final String COMMAND_PREFIX = "/";

    protected final TBot bot;

    public Command(TBot bot) {
        this.bot = bot;
    }

    public boolean addHint() {
        return true;
    }

    public boolean addHelp() {
        return true;
    }

    public abstract String getCommand();
    public abstract String getDescription();
    public abstract String getHelp();
    public abstract void process(CommandMessage msg) throws TelegramApiException;
}
