package org.butch.bots.base.helpers;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;

public class FileHelper {
    private final TelegramLongPollingBot bot;

    public FileHelper(TelegramLongPollingBot bot) {
        this.bot = bot;
    }

    public File getFile(String fileId) throws TelegramApiException {
        return bot.downloadFile(bot.execute(GetFile.builder()
                .fileId(fileId)
                .build()));
    }
}
