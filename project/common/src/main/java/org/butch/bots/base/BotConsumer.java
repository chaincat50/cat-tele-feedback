package org.butch.bots.base;

import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@FunctionalInterface
public interface BotConsumer<TParam> {
    void consume(TParam param) throws TelegramApiException;
}
