package org.butch.bots.base.params2.conditions;

// Can only be last
public class StringCondition implements Condition {
    private final boolean required;

    private String term = "";

    public StringCondition(boolean required) {
        this.required = required;
    }

    public static StringCondition required() {
        return new StringCondition(true);
    }

    public static StringCondition optional() {
        return new StringCondition(false);
    }

    @Override
    public void reset() {
        term = "";
    }

    @Override
    public boolean accept(char c) {
        term += c;
        return true;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public boolean isComplete() {
        return !required || !term.trim().isEmpty();
    }

    @Override
    public String getTerm() {
        return term.trim();
    }
}
