package org.butch.bots.base;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.methods.CopyMessage;
import org.telegram.telegrambots.meta.api.methods.ForwardMessage;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMember;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMember;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Bot extends AbstractBot implements CommandHolder {
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final ArrayList<Command<?>> commands = new ArrayList<>();
    private final HashMap<String, Command<?>> commandsByName = new HashMap<>();

    private User me;

    public User getBotUser() {
        return me;
    }

    protected void addCommand(Command<?> command) {
        commands.add(command);
        commandsByName.put(command.getCommand(), command);
    }

    @Override
    public List<Command<?>> getCommands() {
        return commands;
    }

    public boolean sendMessageOrError(String chatId, String message) {
        try {
            sendMessage(chatId, message);
            return true;
        } catch (TelegramApiException ex) {
            onError(chatId, ex);
            return false;
        }
    }

    public boolean sendReplyOrError(String chatId, Integer replyToId, String message) {
        try {
            sendReply(chatId, replyToId, message);
            return true;
        } catch (TelegramApiException e) {
            onError(chatId, replyToId, e);
            return false;
        }
    }

    public Message sendMessage(String chatId, String message) throws TelegramApiException {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("sendMessage(%s, %s)", chatId, message));
        }
        final SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(message);
        sendMessage.setDisableWebPagePreview(true);
        return execute(sendMessage);
    }

    public Message sendHtml(String chatId, String message) throws TelegramApiException {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("sendHtml(%s, %s)", chatId, message));
        }
        final SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(message);
        sendMessage.setParseMode("HTML");
        sendMessage.setDisableWebPagePreview(true);
        return execute(sendMessage);
    }

    public Message sendMarkdown(String chatId, String message) throws TelegramApiException {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("sendMarkdown(%s, %s)", chatId, message));
        }
        final SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(message);
        sendMessage.setParseMode("MarkdownV2");
        sendMessage.setDisableWebPagePreview(true);
        return execute(sendMessage);
    }

    public String forwardMessage(String chatId, Message message) throws TelegramApiException {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("forwardMessage(%s, %s)", chatId, message));
        }
        final ForwardMessage forwardMessage = ForwardMessage.builder()
                .chatId(chatId)
                .fromChatId(Long.toString(message.getChatId()))
                .messageId(message.getMessageId())
                .build();
        return Integer.toString(execute(forwardMessage).getMessageId());
    }

    public void sendReply(String chatId, Integer replyToId, String message) throws TelegramApiException {
        sendReply(chatId, replyToId, message, true);
    }

    public void sendReply(String chatId, Integer replyToId, String message,
                          boolean disablePreview) throws TelegramApiException {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("sendMessage(%s, %s)", chatId, message));
        }
        final SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setReplyToMessageId(replyToId);
        sendMessage.setText(message);
        sendMessage.setDisableWebPagePreview(disablePreview);
        execute(sendMessage);
    }

    public void sendReplyHtml(String chatId, Integer replyToId, String message) throws TelegramApiException {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("sendMessage(%s, %s)", chatId, message));
        }
        final SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setReplyToMessageId(replyToId);
        sendMessage.setText(message);
        sendMessage.setParseMode("HTML");
        sendMessage.setDisableWebPagePreview(true);
        execute(sendMessage);
    }

    public String copyMessage(String chatId, Message message) throws TelegramApiException {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("copyMessage(%s, %s)", chatId, message));
        }
        final CopyMessage copyMessage = CopyMessage.builder()
                .chatId(chatId)
                .fromChatId(Long.toString(message.getChatId()))
                .messageId(message.getMessageId())
                .build();
        return Long.toString(execute(copyMessage).getMessageId());
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean checkForAdmin(Chat chat, User user) throws TelegramApiException {
        if (!isAllowedAdminActions(chat, user)) {
            sendMessage(chat.getId().toString(),
                    "Эта команда разрешена только для администраторов.");
            return false;
        }
        return true;
    }

    public boolean isAllowedAdminActions(Chat chat, User user) throws TelegramApiException {
        if (chat.getType().equals("private"))
            return true;
        final ChatMember chatMember = getChatMember(chat.getId().toString(), user.getId());
        return chatMember.getStatus().equals("creator") ||
                chatMember.getStatus().equals("administrator");
    }

    public ChatMember getChatMember(String chatId, Long userId) throws TelegramApiException {
        LOGGER.trace(String.format("getChatMember(%s, %s)", chatId, userId));
        final GetChatMember request = new GetChatMember();
        request.setChatId(chatId);
        request.setUserId(userId);
        return execute(request);
    }

    public File getFile(String fileId) throws TelegramApiException {
        return downloadFile(execute(GetFile.builder()
                .fileId(fileId)
                .build()));
    }

    // Return true if you want bot to reply error commands
    public boolean replyErrors() {
        return false;
    }

    @Override
    public void onRegister() {
        try {
            LOGGER.info("Updating list of commands.");
            final List<BotCommand> list = new ArrayList<>();
            for (Command<?> command : commands) {
                if (!command.addHint())
                    continue;
                list.add(
                        BotCommand.builder()
                                .command(command.getCommand())
                                .description(command.getDescription())
                                .build()
                );
            }
            if (!setMyCommands(list)) {
                LOGGER.warn("Could not update list of commands.");
            }
        } catch (TelegramApiException e) {
            LOGGER.warn("Telegram exception: " + e, e);
        }
        try {
            me = getMe();
        } catch (TelegramApiException e) {
            LOGGER.error("Telegram exception: " + e, e);
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            final Message message = update.getMessage();
            final String chatId = Long.toString(message.getChatId());
            if (message.hasText()) {
                final String text = update.getMessage().getText();
                final BaseMessage msg = MessageParser.parseMessage(chatId, update, text);
                if (msg.getType() == Type.TEXT) {
                    final TextMessage textMessage = (TextMessage) msg;
                    processText(textMessage);
                } else if (msg.getType() == Type.COMMAND) {
                    final CommandMessage commandMessage = (CommandMessage) msg;
                    if (commandMessage.getBot() != null &&
                            !commandMessage.getBot().isEmpty() &&
                            !commandMessage.getBot().equalsIgnoreCase(me.getUserName())) {
                        return; // Command for a different bot
                    }

                    if (!processCommand(commandMessage)) {
                        processUnknownCommand(commandMessage);
                    }
                }
            } else if (message.hasPoll()) {
                processPoll(new PollMessage(chatId, update));
            } else {
                processOtherMessage(new OtherMessage(chatId, update));
            }

            for (User user : message.getNewChatMembers()) {
                processNewGroupUser(chatId, user);
            }
            if (message.getLeftChatMember() != null) {
                processLeftGroupUser(chatId, message.getLeftChatMember());
            }
        } else {
            processOtherUpdate(new OtherUpdate(update));
        }
    }

    protected boolean processCommand(CommandMessage msg) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("processCommand: " + msg);
        }
        final Command<?> command = commandsByName.get(msg.getCommand());
        if (command == null)
            return false;

        try {
            processCommandMessage(command, msg);
        } catch (TelegramApiException ex) {
            if (replyErrors()) {
                onError(msg.getChatId(), msg.getMessage().getMessageId(), ex);
            } else {
                onError(msg.getChatId(), ex);
            }
        }
        return true;
    }

    protected void processCommandMessage(Command<?> command, CommandMessage msg) throws TelegramApiException {
        command.process(msg);
    }

    protected void processUnknownCommand(CommandMessage msg) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("processUnknownCommand: " + msg);
        }
        try {
            sendMessage(msg.getChatId(),
                    "Неизвестная команда: " + msg.getCommand());
        } catch (TelegramApiException ex) {
            if (replyErrors()) {
                onError(msg.getChatId(), msg.getMessage().getMessageId(), ex);
            } else {
                onError(msg.getChatId(), ex);
            }
        }
    }

    protected void processText(TextMessage msg) {
    }

    protected void processPoll(PollMessage msg) {
    }

    protected void processOtherMessage(OtherMessage msg) {
    }

    protected void processOtherUpdate(OtherUpdate msg) {
    }

    protected void processNewGroupUser(String chatId, User user) {
    }

    protected void processLeftGroupUser(String chatId, User user) {
    }

    protected void onError(String chatId, TelegramApiException e) {
        LOGGER.warn("Telegram exception: " + e.toString(), e);
        try {
            sendMessage(chatId, e.getMessage());
        } catch (TelegramApiException ex) {
            LOGGER.warn("Could not send error: " + ex, ex);
        }
    }

    protected void onError(String chatId, Integer replyToId, TelegramApiException e) {
        LOGGER.warn("Telegram exception: " + e.toString(), e);
        try {
            sendReply(chatId, replyToId, e.getMessage());
        } catch (TelegramApiException ex) {
            LOGGER.warn("Could not send error: " + ex, ex);
        }
    }

    protected void onError(String chatId, Integer replyToId, String text, TelegramApiException e) {
        LOGGER.warn("Telegram exception: " + text + ": " + e.toString(), e);
        try {
            sendReply(chatId, replyToId, text + ": " + e.getMessage());
        } catch (TelegramApiException ex) {
            LOGGER.warn("Could not send error: " + ex, ex);
        }
    }

    private boolean setMyCommands(List<BotCommand> list) throws TelegramApiException {
        if (list.isEmpty())
            return true;
        final SetMyCommands request = new SetMyCommands();
        request.setCommands(list);
        return execute(request);
    }
}
