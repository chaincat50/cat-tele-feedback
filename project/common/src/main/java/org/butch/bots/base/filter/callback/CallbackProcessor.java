package org.butch.bots.base.filter.callback;

import org.butch.bots.base.Bot2;
import org.butch.bots.base.BotConsumer;
import org.butch.bots.base.filter.Filter;
import org.butch.bots.base.messages.Callback;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class CallbackProcessor implements Filter {
    private final BotConsumer<Callback> consumer;

    private Bot2 bot;

    public CallbackProcessor(@NotNull BotConsumer<Callback> consumer) {
        this.consumer = consumer;
    }

    @Override
    @NotNull
    public String getName() {
        return "CallbackProcessor";
    }

    @Override
    public void accept(@NotNull Update update) throws TelegramApiException {
        if (!update.hasCallbackQuery())
            return;
        final CallbackQuery callbackQuery = update.getCallbackQuery();

        final AnswerCallbackQuery answer = new AnswerCallbackQuery();
        answer.setCallbackQueryId(callbackQuery.getId());
        bot.execute(answer);

        consumer.consume(new Callback(update, callbackQuery.getData()));
    }

    @Override
    public boolean register(@NotNull Bot2 bot) throws TelegramApiException {
        this.bot = bot;
        return true;
    }
}
