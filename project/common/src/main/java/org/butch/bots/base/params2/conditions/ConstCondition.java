package org.butch.bots.base.params2.conditions;

import org.butch.bots.base.params2.ParamUtils;

import java.util.List;

public class ConstCondition implements Condition {
    private final boolean required;
    private final List<String> values;

    private String term = "";

    private ConstCondition(boolean required, String... values) {
        this.required = required;
        this.values = List.of(values);
    }

    public static ConstCondition required(String... values) {
        return new ConstCondition(true, values);
    }

    public static ConstCondition optional(String... values) {
        return new ConstCondition(false, values);
    }

    @Override
    public void reset() {
        term = "";
    }

    @Override
    public boolean accept(char c) {
        if (ParamUtils.isSeparator(c))
            return term.isEmpty();
        term += c;
        for (String value : values) {
            if (value.startsWith(term))
                return true;
        }
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public boolean isComplete() {
        for (String value : values) {
            if (value.equals(term))
                return true;
        }
        return false;
    }

    @Override
    public String getTerm() {
        return term;
    }
}
