package org.butch.bots.base.messages;

import org.telegram.telegrambots.meta.api.objects.Update;

public class TextMessage extends BaseMessage {
    private final String text;

    public TextMessage(String chatId, Update update, String text) {
        super(chatId, update);
        this.text = text;
    }

    @Override
    public Type getType() {
        return Type.TEXT;
    }

    public String getText() {
        return text;
    }
}
