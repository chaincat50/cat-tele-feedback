package org.butch.bots.base.messages;

import org.telegram.telegrambots.meta.api.objects.Update;

public class OtherUpdate extends Base {
    public OtherUpdate(Update update) {
        super(update);
    }

    @Override
    public Type getType() {
        return Type.OTHER_UPDATE;
    }
}
