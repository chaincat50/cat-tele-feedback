package org.butch.bots.base.commands;

import org.butch.bots.base.CommandHolder;
import org.butch.bots.base.messages.CommandMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public abstract class AbstractHelpCommand<TBot> extends Command<TBot> {
    public static final String COMMAND = "help";

    private CommandHolder holder = null;

    public AbstractHelpCommand(TBot bot) {
        super(bot);
    }

    public void setHolder(CommandHolder holder) {
        this.holder = holder;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return "Помощь";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        if (holder == null) {
            sendHtml(msg.getChatId(), getHelp());
        } else {
            final StringBuilder builder = new StringBuilder();
            builder.append(getHelp()).append("\n"); // Add this help first
            for (Command<?> command : holder.getCommands()) {
                if (command == this) // Don't print this help like general command
                    continue;
                if (!command.addHelp())
                    continue;
                builder.append(command.getHelp()).append("\n");
            }
            sendHtml(msg.getChatId(), builder.toString());
        }
    }

    protected abstract void sendHtml(String chatId, String text) throws TelegramApiException;
}
