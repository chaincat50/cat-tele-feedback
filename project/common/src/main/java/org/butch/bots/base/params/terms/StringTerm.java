package org.butch.bots.base.params.terms;

public class StringTerm implements Term {
    private final String value;

    public StringTerm(String value) {
        this.value = value;
    }

    @Override
    public TermType getType() {
        return TermType.STRING;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "string: " + value;
    }
}
