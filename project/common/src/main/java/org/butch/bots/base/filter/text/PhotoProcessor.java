package org.butch.bots.base.filter.text;

import org.butch.bots.base.BotConsumer;
import org.butch.bots.base.filter.MessageFilter;
import org.butch.bots.base.messages.OtherMessage;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class PhotoProcessor extends MessageFilter {
    private final BotConsumer<OtherMessage> consumer;

    public PhotoProcessor(@NotNull BotConsumer<OtherMessage> consumer) {
        this.consumer = consumer;
    }

    @Override
    @NotNull
    public String getName() {
        return "PhotoProcessor";
    }

    @Override
    protected void accept(@NotNull Update update,
                          @NotNull Message message,
                          @NotNull String chatId) throws TelegramApiException {
        if (!message.hasPhoto())
            return;
        consumer.consume(new OtherMessage(chatId, update));
    }
}
