package org.butch.bots.base.params.conditions;

import org.butch.bots.base.params.ParamUtils;
import org.butch.bots.base.params.Result;
import org.butch.bots.base.params.terms.StringTerm;
import org.butch.bots.base.params.terms.Term;

public class ConstCondition extends Condition {
    private final String value;
    private int nextIndex = 0;

    public ConstCondition(String value) {
        this.value = value;
    }

    public static ConstCondition create(String value) {
        return new ConstCondition(value);
    }

    @Override
    public Result accept(char c) throws IllegalStateException {
        if (nextIndex < 0)
            return Result.NO_MATCH;
        if (nextIndex == value.length()) {
            if (ParamUtils.isSeparator(c)) {
                return Result.EXACT_MATCH;
            }
            nextIndex = -1;
            return Result.NO_MATCH;
        }
        if (nextIndex > value.length())
            throw new IllegalStateException("const: ExactMatch");

        if (nextIndex == 0 && ParamUtils.isSeparator(c)) {
            return Result.PARTIAL_MATCH;
        }
        if (c != value.charAt(nextIndex)) {
            nextIndex = -1;
            return Result.NO_MATCH;
        }
        ++nextIndex;
        return Result.PARTIAL_MATCH;
    }

    @Override
    public void reset() {
        nextIndex = 0;
    }

    @Override
    public StringTerm getTerm() throws IllegalStateException {
        if (nextIndex < value.length())
            throw new IllegalStateException("const: " + value);
        return new StringTerm(value);
    }

    public boolean matches(Term term) {
        return term instanceof StringTerm && value.equals(((StringTerm) term).getValue());
    }
}
