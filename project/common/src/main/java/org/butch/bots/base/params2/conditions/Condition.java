package org.butch.bots.base.params2.conditions;

public interface Condition {
    void reset();
    // Return true, if it can accept more, false otherwise.
    boolean accept(char c);
    // Is this condition mandatory or can be skipped.
    boolean isRequired();
    // Does this conditions contains a successfully complete term.
    boolean isComplete();

    String getTerm();
}
