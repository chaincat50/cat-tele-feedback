package org.butch.bots.base.filter.inline;

import org.butch.bots.base.BotConsumer;
import org.butch.bots.base.filter.Filter;
import org.butch.bots.base.messages.Inline;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class InlineProcessor implements Filter {
    private final BotConsumer<Inline> consumer;

    public InlineProcessor(@NotNull BotConsumer<Inline> consumer) {
        this.consumer = consumer;
    }

    @Override
    @NotNull
    public String getName() {
        return "InlineProcessor";
    }

    @Override
    public void accept(@NotNull Update update) throws TelegramApiException {
        if (!update.hasInlineQuery())
            return;
        consumer.consume(new Inline(update));
    }
}
