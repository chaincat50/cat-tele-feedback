package org.butch.bots.base;

import org.telegram.telegrambots.meta.api.objects.User;

public class Utils {
    public static String getHtmlUserLink(User user) {
        return String.format("<a href=\"%s\">%s</a>",
                getTgUserLink(user),
                getUsername(user)
        );
    }

    public static String getTgUserLink(User user) {
        return String.format("tg://user?id=%d",
                user.getId()
        );
    }

    public static String getBotUrl(User user) {
        return String.format("http://t.me/%s",
                user.getUserName() // Unlike user, bot always has username
        );
    }

    public static String getUsername(User user) {
        if (user.getLastName() != null)
            return user.getFirstName() + " " + user.getLastName();
        return user.getFirstName();
    }
}
