package org.butch.bots.base.helpers;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;

public class KeyboardHelper {
    private final TelegramLongPollingBot bot;

    public KeyboardHelper(TelegramLongPollingBot bot) {
        this.bot = bot;
    }

    public InlineKeyboardBuilder inline() {
        return new InlineKeyboardBuilder();
    }

    public static class InlineKeyboardBuilder {
        private final InlineKeyboardMarkup result = new InlineKeyboardMarkup();

        public InlineKeyboardBuilder() {
            result.setKeyboard(new ArrayList<>());
            result.getKeyboard().add(new ArrayList<>());
        }

        public InlineKeyboardBuilder button(String text, String data) {
            result.getKeyboard().get(result.getKeyboard().size() - 1).add(
                    InlineKeyboardButton.builder()
                            .text(text)
                            .callbackData(data)
                            .build()
            );
            return this;
        }

        public InlineKeyboardBuilder nextLine() {
            result.getKeyboard().add(new ArrayList<>());
            return this;
        }

        public InlineKeyboardMarkup build() {
            return result;
        }
    }
}
