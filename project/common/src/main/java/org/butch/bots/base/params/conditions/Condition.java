package org.butch.bots.base.params.conditions;

import org.butch.bots.base.params.Result;
import org.butch.bots.base.params.terms.Term;

public abstract class Condition {
    public abstract Result accept(char c) throws IllegalStateException;
    public abstract void reset();
    public abstract Term getTerm() throws IllegalStateException;
}
