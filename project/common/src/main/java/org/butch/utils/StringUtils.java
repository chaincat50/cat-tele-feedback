package org.butch.utils;

public class StringUtils {
    private StringUtils() {}

    public static String join(String separator, Object[] objects) {
        return join(separator, objects, 0, objects.length);
    }

    public static String join(String separator, Object[] objects, int count) {
        return join(separator, objects, 0, count);
    }

    public static String join(String separator, Object[] objects, int from, int till) {
        final StringBuilder builder = new StringBuilder();
        for (int i = from; i < till; ++i) {
            if (i > from) {
                builder.append(separator);
            }
            builder.append(objects[i]);
        }
        return builder.toString();
    }
}
