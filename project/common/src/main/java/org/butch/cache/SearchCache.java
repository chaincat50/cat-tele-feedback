package org.butch.cache;

import java.util.List;

public interface SearchCache<TObj> {
    void load(String text, TObj obj);
    int size();
    List<TObj> search(String mask);
}
