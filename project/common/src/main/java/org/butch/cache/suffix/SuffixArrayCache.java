package org.butch.cache.suffix;

import org.butch.cache.SearchCache;

import java.util.*;

public class SuffixArrayCache<TObj> implements SearchCache<TObj> {
    private final Map<Index, TObj> map = new HashMap<>();

    @Override
    public void load(String text, TObj obj) {
        map.put(new Index(text.toLowerCase(Locale.ROOT)), obj);
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public List<TObj> search(String mask) {
        final String maskLC = mask.toLowerCase(Locale.ROOT);
        final List<TObj> ret = new ArrayList<>();
        map.forEach((k, v) -> {
            if (k.contains(maskLC)) {
                ret.add(v);
            }
        });
        return ret;
    }
}
