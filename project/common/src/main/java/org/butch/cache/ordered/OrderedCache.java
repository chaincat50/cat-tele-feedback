package org.butch.cache.ordered;

import org.butch.cache.SearchCache;

import java.util.Comparator;
import java.util.List;

public class OrderedCache<TObj> implements SearchCache<TObj> {
    private final SearchCache<TObj> underlying;
    private final Comparator<TObj> comparator;

    public OrderedCache(SearchCache<TObj> underlying, Comparator<TObj> comparator) {
        this.underlying = underlying;
        this.comparator = comparator;
    }

    @Override
    public void load(String text, TObj obj) {
        underlying.load(text, obj);
    }

    @Override
    public int size() {
        return underlying.size();
    }

    @Override
    public List<TObj> search(String mask) {
        final List<TObj> ret = underlying.search(mask);
        ret.sort(comparator);
        return ret;
    }
}
