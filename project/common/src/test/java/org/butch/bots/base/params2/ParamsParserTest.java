package org.butch.bots.base.params2;

import org.butch.bots.base.params2.conditions.Condition;
import org.butch.bots.base.params2.conditions.ConstCondition;
import org.butch.bots.base.params2.conditions.IntegerCondition;
import org.butch.bots.base.params2.conditions.StringCondition;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ParamsParserTest {
    @Test(timeout = 2000)
    public void testIntegerParam() {
        final Condition[] conditions = new Condition[] {
                IntegerCondition.required()
        };

        checkNotEnoughData(ParamsParser.parse("", conditions));
        checkResults(ParamsParser.parse("123", conditions), "123");
        checkParsingError(ParamsParser.parse("q", conditions), "q");
        checkParsingError(ParamsParser.parse("1q", conditions), "1q");
        checkResults(ParamsParser.parse("   123    ", conditions), "123");
        checkParsingError(ParamsParser.parse("123  q", conditions), "q");
    }

    @Test(timeout = 2000)
    public void testConstParam() {
        final Condition[] conditions = new Condition[] {
                ConstCondition.required("all", "any")
        };

        checkNotEnoughData(ParamsParser.parse("", conditions));
        checkResults(ParamsParser.parse("all", conditions), "all");
        checkResults(ParamsParser.parse("any", conditions), "any");
        checkParsingError(ParamsParser.parse("q", conditions), "q");
        checkParsingError(ParamsParser.parse("allq", conditions), "allq");
        checkResults(ParamsParser.parse("   all    ", conditions), "all");
        checkParsingError(ParamsParser.parse("all  q", conditions), "q");
    }

    @Test(timeout = 2000)
    public void testComplexCondition1() {
        final Condition[] conditions = new Condition[] {
                ConstCondition.required("all", "white", "red"),
                ConstCondition.optional("region1", "region2"),
                StringCondition.required()
        };

        checkNotEnoughData(ParamsParser.parse("", conditions));
        checkNotEnoughData(ParamsParser.parse("all", conditions));
        checkNotEnoughData(ParamsParser.parse("all region1", conditions));
        checkResults(ParamsParser.parse("all   q", conditions), "all", null, "q");
        checkResults(ParamsParser.parse("red  region1  q", conditions), "red", "region1", "q");
        checkParsingError(ParamsParser.parse("allregion1 region2 q", conditions), "allr");
        checkResults(ParamsParser.parse("all region1q", conditions), "all", null, "region1q");
    }

    private void checkNotEnoughData(Result result) {
        checkResults(result, ResultStatus.NOT_ENOUGH_DATA, null);
    }

    private void checkParsingError(Result result, String error) {
        checkResults(result, ResultStatus.PARSING_ERROR, error);
    }

    private void checkResults(Result result, ResultStatus status, String error) {
        Assert.assertEquals(status, result.getStatus());
        Assert.assertEquals(error, result.getError());
    }

    private void checkResults(Result result, String... expected) {
        Assert.assertEquals(ResultStatus.OK, result.getStatus());
        Assert.assertNull(result.getError());
        checkResults(result.getTerms(), expected);
    }

    private void checkResults(List<String> result, String... expected) {
        Assert.assertEquals(expected.length, result.size());
        for (int i = 0; i < expected.length; ++i) {
            Assert.assertEquals(expected[i], result.get(i));
        }
    }
}
