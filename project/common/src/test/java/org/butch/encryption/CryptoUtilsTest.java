package org.butch.encryption;

import org.junit.Assert;
import org.junit.Test;

import javax.crypto.SecretKey;

public class CryptoUtilsTest {
    @Test(timeout = 1000L)
    public void testHexUnhex() throws Exception {
        final byte[] expected = {
                (byte) 0,
                (byte) 1,
                (byte) 2,
                (byte) 16,
                (byte) 17,
                (byte) 33,
                (byte) 78,
                (byte) 127,
                (byte) 128,
                (byte) 129,
                (byte) 196,
                (byte) 255
        };

        final byte[] acual = CryptoUtils.unhex(CryptoUtils.hex(expected));
        Assert.assertArrayEquals(expected, acual);
    }

    @Test(timeout = 1000L)
    public void testEncryptDecrypt() throws Exception {
        final String expected = "an0vw9b54[a3v7[we09bvr[aw038rabv[w0t] -38VA[B8";
        final SecretKey key = CryptoUtils.getAESKey();
        final String encrypted = CryptoUtils.encrypt(key, expected);
        final String actual = CryptoUtils.decrypt(key, encrypted);
        Assert.assertEquals(expected, actual);
    }
}
