package org.butch.cache.suffix;

import org.junit.Assert;
import org.junit.Test;

public class IndexTest {
    @Test(timeout = 1000)
    public void testIndex() {
        Index index = new Index("qwertyu\nasdfghasd");

        Assert.assertTrue(index.contains(""));
        Assert.assertTrue(index.contains("qwe"));
        Assert.assertTrue(index.contains("we"));
        Assert.assertTrue(index.contains("asd"));
        Assert.assertTrue(index.contains("sd"));

        Assert.assertFalse(index.contains("ds"));
        Assert.assertFalse(index.contains("qweq"));
        Assert.assertFalse(index.contains("qwertyu\nasdfghasdq"));
    }

    @Test(timeout = 1000)
    public void testSameSymbol() {
        Index index = new Index("aaaaa");

        Assert.assertTrue(index.contains(""));
        Assert.assertTrue(index.contains("a"));
        Assert.assertTrue(index.contains("aa"));
        Assert.assertTrue(index.contains("aaa"));
        Assert.assertTrue(index.contains("aaaa"));
        Assert.assertTrue(index.contains("aaaaa"));

        Assert.assertFalse(index.contains("aaaaaa"));
        Assert.assertFalse(index.contains("b"));
    }
}
