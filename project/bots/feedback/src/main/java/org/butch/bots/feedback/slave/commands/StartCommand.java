package org.butch.bots.feedback.slave.commands;

import org.butch.bots.base.commands.BaseStartCommand;
import org.butch.bots.feedback.Constants;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.telegram.telegrambots.meta.api.objects.User;

public class StartCommand extends BaseStartCommand<BaseSlaveBot> {
    public StartCommand(BaseSlaveBot bot) {
        super(bot);
    }

    @Override
    protected String getUserGreeting(User user, String userLink) {
        final String welcome = bot.getData().getWelcome();
        if (welcome == null || welcome.isEmpty())
            return welcome;
        return welcome.replace(Constants.USER_PLACEHOLDER, userLink);
    }
}
