package org.butch.bots.feedback.slave.commands;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.feedback.Constants;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class ChangeGreetingCommand extends BaseOwnerCommand {
    public ChangeGreetingCommand(BaseSlaveBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "greet";
    }

    @Override
    public String getDescription() {
        return "[текст] Напечатать/задать текст приветствия";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [текст]\n" +
                "Задаёт текст приветствия пользователя, либо печатает текущий.\n" +
                "  <b>текст</b> - Новый текст приветствия. " +
                "Если параметр отсутствует, будет напечатан текущий текст. " +
                "Специальная последовательность <i>" + Constants.USER_PLACEHOLDER +
                "</i> будет заменена на имя пользователя. " +
                "Пример: <i>Привет, " + Constants.USER_PLACEHOLDER + "!</i>\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        final String text = msg.getParams();
        if (text.trim().isEmpty()) {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Текущий текст приветствия: \n\n" +
                    bot.getData().getWelcome());
            return;
        }
        final Result result = bot.changeGreeting(userId, text);
        if (result == Result.OK) {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Текст приветствия успешно изменён."
            );
        } else {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Не удалось изменить текст приветствия. " + result.getDescription()
            );
        }
    }
}
