package org.butch.bots.feedback;

public enum Result {
    OK,
    ERROR,
    NOT_OWNER,
    DB_ERROR,
    NOT_FOUND,
    CRITICAL,
    NO_MESSAGE,
    BAN_ERROR,
    ALREADY_BANNED,
    IN_PROGRESS;

    public String getDescription() {
        switch (this) {
            case OK:
                return null;

            case ERROR:
                return "Произошла неожиданная ошибка. " +
                        "Возможно, она вызвана неполадками в телеграмме. " +
                        "Попробуйте повторить позже, если не поможет - " +
                        "свяжитесь в автором бота.";

            case NOT_OWNER:
                return "Данная команда разрешена только для владельца бота.";

            case DB_ERROR:
                return "Ошибка базы данных.";

            case NOT_FOUND:
                return "Бот не найден.";

            case CRITICAL:
                return "Критическая ошибка в коде бота.";

            case NO_MESSAGE:
                return "Не получилось найти оригинальное сообщение. " +
                        "Попробуйте ответить на более новое сообщение пользователя.";

            case BAN_ERROR:
                return "Эту команду надо выполнить в ответ на сообщение пользователя, " +
                        "которого вы хотите забанить.";

            case ALREADY_BANNED:
                return "Пользователь уже забанен.";

            case IN_PROGRESS:
                return "Изменения в процессе. Дождитесь окончания выполнения предыдущей операции.";
        }

        return "Неизвестная ошибка.";
    }
}
