package org.butch.bots.feedback.slave;

import org.butch.bots.base.Bot;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.master.FeedbackMasterBot;
import org.butch.bots.feedback.master.repository.BanDto;
import org.butch.bots.feedback.master.repository.BotDto;
import org.butch.bots.feedback.slave.commands.*;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChat;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.BotSession;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public abstract class BaseSlaveBot extends Bot {
    private final FeedbackMasterBot masterBot;
    private final BotDto data;
    private BotSession session;
    private String userName;

    private final ConcurrentHashMap<String, BanDto> bans = new ConcurrentHashMap<>();

    public BaseSlaveBot(FeedbackMasterBot masterBot, BotDto data) {
        this.masterBot = masterBot;
        this.data = data;

        addCommand(new StartCommand(this));
        addCommand(new UnregisterCommand(this));
        addCommand(new ChangeGreetingCommand(this));
        addCommand(new ChangeChatCommand(this));
        addCommand(new ChangeOwnerCommand(this));
        addCommand(new BanCommand(this));
        addCommand(new UnbanAllCommand(this));
    }

    @Override
    public void onClosing() {
        super.onClosing();

        try {
            masterBot.restart(this);
        } catch (TelegramApiException ex) {
            LOGGER.warn(ex.toString()); // TODO: Send error message?
        }
    }

    @Override
    public String getBotToken() {
        return data.getToken();
    }

    public FeedbackMasterBot getMasterBot() {
        return masterBot;
    }

    public BotDto getData() {
        return data;
    }

    public boolean isOwner(String userId) {
        return Objects.equals(data.getOwnerId(), userId);
    }

    public void setSession(BotSession session) {
        this.session = session;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void addBanFromDatabase(BanDto banDto) {
        bans.put(banDto.getUserId(), banDto);
    }

    public Result unregister(String userId) {
        if (!isOwner(userId))
            return Result.NOT_OWNER;
        final Result ret = masterBot.unregister(this);
        if (ret != Result.OK)
            return ret;

        unbanAll(userId); // Cleanup
        session.stop();
        return Result.OK;
    }

    public void stop() {
        session.stop();
    }

    public Result changeGreeting(String userId, String text) {
        if (!isOwner(userId))
            return Result.NOT_OWNER;
        try {
            final BotDto clone = getData().clone();
            clone.setWelcome(text);
            final Result ret = masterBot.updateBot(clone);
            if (ret != Result.OK)
                return ret;

            this.data.setWelcome(text);
            return Result.OK;
        } catch (CloneNotSupportedException ex) {
            LOGGER.error("changeGreeting", ex);
            return Result.CRITICAL;
        }
    }

    public Result changeChat(String userId, String chatId) {
        if (!isOwner(userId))
            return Result.NOT_OWNER;
        try {
            final BotDto clone = getData().clone();
            clone.setChatId(chatId);
            final Result ret = masterBot.updateBot(clone);
            if (ret != Result.OK)
                return ret;

            this.data.setChatId(chatId);
            return Result.OK;
        } catch (CloneNotSupportedException ex) {
            LOGGER.error("changeChat", ex);
            return Result.CRITICAL;
        }
    }

    public Result changeOwner(String fromUserId, String toUserId) {
        if (!isOwner(fromUserId))
            return Result.NOT_OWNER;
        try {
            final BotDto clone = getData().clone();
            clone.setOwnerId(toUserId);
            final Result ret = masterBot.updateBot(clone);
            if (ret != Result.OK)
                return ret;

            this.data.setOwnerId(toUserId);
            return Result.OK;
        } catch (CloneNotSupportedException ex) {
            LOGGER.error("changeOwner", ex);
            return Result.CRITICAL;
        }
    }

    public Result banUser(String fromUserId, Message original, String reason) throws TelegramApiException {
        if (!isOwner(fromUserId))
            return Result.NOT_OWNER;

        if (original == null)
            return Result.BAN_ERROR;
        if (!Objects.equals(original.getFrom().getId(), getBotUser().getId()))
            return Result.BAN_ERROR;

        final String originalChatId = masterBot.getChatId(
                Long.toString(original.getChatId()),
                Integer.toString(original.getMessageId()));
        if (originalChatId == null)
            return Result.NO_MESSAGE;

        final GetChat getChat = GetChat.builder()
                .chatId(originalChatId)
                .build();
        final Chat chat = execute(getChat);

        final StringBuilder username = new StringBuilder(chat.getFirstName());
        if (chat.getLastName() != null) {
            username.append(" ").append(chat.getLastName());
        }
        if (chat.getUserName() != null) {
            username.append(" ( ").append(chat.getUserName()).append(" )");
        }

        BanDto banDto = bans.get(originalChatId);
        if (banDto != null)
            return Result.ALREADY_BANNED;

        banDto = new BanDto();
        banDto.setBotId(data.getId());
        banDto.setUserId(originalChatId);
        banDto.setName(username.toString());
        banDto.setReason(reason);

        final Result result = masterBot.insertBan(banDto);
        if (result == Result.OK) {
            bans.put(banDto.getUserId(), banDto);
        }
        return result;
    }

    // TODO: List bans, unban

    public Result unbanAll(String fromUserId) {
        if (!isOwner(fromUserId))
            return Result.NOT_OWNER;

        for (BanDto banDto : bans.values()) {
            masterBot.deleteBan(banDto);
        }
        bans.clear();
        return Result.OK;
    }

    public boolean isBanned(String userId) {
        return bans.containsKey(userId);
    }
}
