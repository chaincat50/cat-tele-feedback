package org.butch.bots.feedback.master.repository;

public class BanDto extends CommonDto {
    private int botId;
    private String userId;
    private String name;
    private String reason;

    public int getBotId() {
        return botId;
    }

    public void setBotId(int botId) {
        this.botId = botId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "" +
                " id = " + getId() +
                " botId = " + botId +
                " userId = " + userId +
                " name = " + name +
                " reason = " + reason
                ;
    }
}
