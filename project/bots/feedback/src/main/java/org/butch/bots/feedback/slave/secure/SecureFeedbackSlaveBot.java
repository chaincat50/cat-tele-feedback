package org.butch.bots.feedback.slave.secure;

import org.butch.bots.base.Utils;
import org.butch.bots.base.messages.BaseMessage;
import org.butch.bots.base.messages.OtherMessage;
import org.butch.bots.base.messages.PollMessage;
import org.butch.bots.base.messages.TextMessage;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.TelegramConsumer;
import org.butch.bots.feedback.master.FeedbackMasterBot;
import org.butch.bots.feedback.master.repository.BotDto;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.butch.utils.Randomizer;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Objects;

public class SecureFeedbackSlaveBot extends BaseSlaveBot {
    private static final String SENDER_PREFIX = "sender:";
    private static final String CHAT_ID_PREFIX = "chatId:";
    private static final String MESSAGE_PREFIX = "message:";

    private final Randomizer rnd = new Randomizer();

    public SecureFeedbackSlaveBot(FeedbackMasterBot masterBot, BotDto data) throws NoSuchAlgorithmException, NoSuchProviderException {
        super(masterBot, data);

        addCommand(new HelpCommand(this));
        addCommand(new UnsecureCommand(this));
    }

    @Override
    public String getBotUsername() {
        return "SecureFeedbackSlaveBot #" + getData().getId();
    }

    @Override
    public void onUpdateReceived(Update update) {
        super.onUpdateReceived(update);

        if (!update.hasMessage())
            return;

        final Message message = update.getMessage();
        final String userId = Long.toString(update.getMessage().getFrom().getId());
        final String chatId = Long.toString(update.getMessage().getChatId());

        if (isBanned(userId) || isBanned(chatId))
            return; // Ignore banned users and chats completely

        if (Objects.equals(userId, chatId) &&
                !Objects.equals(getData().getChatId(), chatId)) {
            // Personal message - forward to the chat
            try {
                if (hasValidPhoto(message)) {
                    final SendPhoto sendPhoto = SendPhoto.builder()
                            .chatId(getData().getChatId())
                            .photo(new InputFile(message.getPhoto().get(0).getFileId()))
                            .parseMode("HTML")
                            .caption(generateText(message,
                                    message.getCaption() != null
                                            ? message.getCaption()
                                            : "<i>Сообщение отсутствует.</i>"
                                    )
                            )
                            .build();
                    execute(sendPhoto);
                } else if (hasValidText(message)) {
                    final SendMessage sendMessage = SendMessage.builder()
                            .chatId(getData().getChatId())
                            .parseMode("HTML")
                            .text(generateText(message, message.getText()))
                            .build();
                    execute(sendMessage);
                } else {
                    sendReply(chatId, message.getMessageId(), "" +
                            "Отправка данного типа сообщений не поддерживается. " +
                            "Вы можете отправить текст и/или изображение. " +
                            "Длина текста в одном сообщении не может превышать " +
                            Limits.MAX_TEXT_LENGTH + " символов. " +
                            "Длина подписи к изображению не может превышать " +
                            Limits.MAX_PHOTO_CAPTION_LENGTH + " символов. ");
                }
            } catch (TelegramApiException ex) {
                onError(chatId, ex);
            }
        }
    }

    private boolean hasValidPhoto(Message msg) {
        if (!msg.hasPhoto())
            return false;
        if (msg.getPhoto().size() <= 0)
            return false;
        return msg.getCaption() == null ||
                msg.getCaption().length() <= Limits.MAX_PHOTO_CAPTION_LENGTH;
    }

    private boolean hasValidText(Message msg) {
        return msg.hasText() && msg.getText().length() <= Limits.MAX_TEXT_LENGTH;
    }

    @Override
    protected void processText(TextMessage msg) {
        processChatMessage(msg);
    }

    @Override
    protected void processPoll(PollMessage msg) {
        processChatMessage(msg);
    }

    @Override
    protected void processOtherMessage(OtherMessage msg) {
        processChatMessage(msg);
    }

    // We need this to avoid sending bot commangs to the users.
    private void processChatMessage(BaseMessage msg) {
        final Update update = msg.getUpdate();

        final Message message = update.getMessage();
        final String chatId = Long.toString(update.getMessage().getChatId());

        if (Objects.equals(chatId, getData().getChatId())) {
            // Message in configured chat
            final Message original = message.getReplyToMessage();
            if (original != null && Objects.equals(original.getFrom().getId(), getBotUser().getId())) {
                // This is a reply to a bots message
                final String originalChatId = getChatId(original);
                try {
                    if (originalChatId == null) {
                        sendMessage(chatId, "" +
                                "Не получилось найти идентификатор чата " +
                                "в оригинальном сообщении. " +
                                "Ответ не будет отправлен.");
                    } else {
                        copyMessage(originalChatId, message);
                    }
                } catch (TelegramApiException ex) {
                    onError(chatId, ex);
                }
            }
        }
    }

    private String generateText(Message msg, String text) {
        return String.format("" +
                        "<b>%s</b> %s\n" +
                        "<b>%s</b> %d\n" +
                        "<b>%s</b> \n\n" +
                        "%s\n\n" +
                        "<i>---\n\n" +
                        "%s</i>",
                SENDER_PREFIX, Utils.getHtmlUserLink(msg.getFrom()),
                CHAT_ID_PREFIX, msg.getChatId(),
                MESSAGE_PREFIX, text,
                rnd.nextString(Limits.MIN_SALT_LENGTH, Limits.MAX_SALT_LENGTH)
        );
    }

    private static String getChatId(Message msg) {
        if (msg.hasText())
            return getChatId(msg.getText());
        if (msg.getCaption() != null)
            return getChatId(msg.getCaption());
        return null;
    }

    private static String getChatId(String text) {
        final String[] lines = text.split("\n");
        if (lines.length < 3)
            return null;
        if (!lines[0].startsWith(SENDER_PREFIX) ||
                !lines[1].startsWith(CHAT_ID_PREFIX) ||
                !lines[2].startsWith(MESSAGE_PREFIX))
            return null;
        try {
            return Long.toString(Long.parseLong(lines[1].substring(CHAT_ID_PREFIX.length() + 1)));
        } catch (Throwable ex) {
            return null;
        }
    }

    public void unsecure(String userId, TelegramConsumer<Result> consumer) throws TelegramApiException {
        if (!isOwner(userId)) {
            consumer.consume(Result.NOT_OWNER);
            return;
        }

        getMasterBot().unsecure(this, consumer);
    }
}
