package org.butch.bots.feedback.master.repository;

import java.util.List;

public interface Storage {
    void init();

    int insertBot(CommonDao dao);
    boolean updateBot(CommonDao dao);
    boolean deleteBot(CommonDao dao);
    List<CommonDao> queryBots();

    // ChatId by message's unique key.
    // We have to store it, since Telegram Bot API
    // doesn't provide a way to obtain this information.
    boolean insertChatId(String key, String chatId);
    String getChatId(String key);
    void deleteOldChatIds();

    int insertBan(CommonDao dao);
    boolean deleteBan(CommonDao dao);
    List<CommonDao> queryBans();
}
