package org.butch.bots.feedback.slave.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.feedback.slave.BaseSlaveBot;

public abstract class OwnerCommand<TBot extends BaseSlaveBot> extends Command<TBot> {
    public OwnerCommand(TBot bot) {
        super(bot);
    }

    @Override
    public boolean addHint() {
        return false;
    }
}
