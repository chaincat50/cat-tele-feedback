package org.butch.bots.feedback.slave.commands;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class UnregisterCommand extends BaseOwnerCommand {
    public UnregisterCommand(BaseSlaveBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "unregister";
    }

    @Override
    public String getDescription() {
        return "Разрегистрировать бота";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Забирает у мастера управление ботом. " +
                "При этом все данные о боте, включая настройки, удаляются.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        final Result result = bot.unregister(userId);
        if (result == Result.OK) {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Бот успешно разрегистрирован. " +
                    "Все записи о нём удалены из базы данных. " +
                    "Мастер больше не управляет этим ботом."
            );
        } else {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Не удалось разрегистрировать бота. " + result.getDescription()
            );
        }
    }
}
