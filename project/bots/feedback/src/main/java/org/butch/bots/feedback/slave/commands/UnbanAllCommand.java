package org.butch.bots.feedback.slave.commands;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class UnbanAllCommand extends BaseOwnerCommand {
    public UnbanAllCommand(BaseSlaveBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "unban_all";
    }

    @Override
    public String getDescription() {
        return "Удалить все баны";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Полностью очищает список забаненных пользователей.\n" +
                "Это временная команда. В дальнейшем она будет заменена " +
                "на более удобный формат.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        final Result result = bot.unbanAll(userId);
        if (result == Result.OK) {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Список забаненных пользователей очищен."
            );
        } else {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Не удалось очистить список забаненных пользователей. " +
                    result.getDescription()
            );
        }
    }
}
