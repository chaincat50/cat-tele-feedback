package org.butch.bots.feedback.master.commands;

import org.butch.bots.base.commands.BaseStartCommand;
import org.butch.bots.feedback.master.FeedbackMasterBot;

public class StartCommand extends BaseStartCommand<FeedbackMasterBot> {
    public StartCommand(FeedbackMasterBot bot) {
        super(bot);
    }
}
