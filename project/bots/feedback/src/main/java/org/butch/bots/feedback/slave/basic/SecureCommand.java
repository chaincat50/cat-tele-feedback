package org.butch.bots.feedback.slave.basic;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.slave.commands.OwnerCommand;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class SecureCommand extends OwnerCommand<FeedbackSlaveBot> {
    public SecureCommand(FeedbackSlaveBot bot) {
        super(bot);
    }

    @Override
    public boolean addHelp() {
        return false;
    }

    @Override
    public String getCommand() {
        return "secure";
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Меняет бота на более безопасного, " +
                "но с меньшими возможностями.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        bot.secure(userId, result -> {
            if (result == Result.OK) {
                bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                        "Тип бота успешно изменён. Теперь бот более безопасен для общения, " +
                        "но обладает меньшими возможностями."
                );
            } else {
                bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                        "Не удалось изменить тип бота. " + result.getDescription()
                );
            }
        });
    }
}
