package org.butch.bots.feedback.master.repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BotDto extends CommonDto implements Cloneable {
    private String ownerId;
    private String token;
    private String chatId;
    private String welcome;
    private boolean isSecure = false;

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getWelcome() {
        return welcome;
    }

    public void setWelcome(String welcome) {
        this.welcome = welcome;
    }

    public boolean getIsSecure() {
        return isSecure;
    }

    public void setIsSecure(boolean isSecure) {
        this.isSecure = isSecure;
    }

    @Override
    public BotDto clone() throws CloneNotSupportedException {
        final BotDto ret = (BotDto) super.clone();
        ret.setId(getId());
        ret.setOwnerId(getOwnerId());
        ret.setChatId(getChatId());
        ret.setWelcome(getWelcome());
        return ret;
    }

    public String log() {
        return "" +
                " id = " + getId() +
                " chatId = " + chatId +
                " isSecure = " + isSecure
                ;
    }

    @Override
    public String toString() {
        return "" +
                " id = " + getId() +
                " ownerId = " + ownerId +
                " chatId = " + chatId +
                " welcome = " + welcome +
                " isSecure = " + isSecure
                ;
    }
}
