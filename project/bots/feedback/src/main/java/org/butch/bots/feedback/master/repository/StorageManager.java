package org.butch.bots.feedback.master.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.butch.encryption.CryptoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;
import java.util.ArrayList;
import java.util.List;

public class StorageManager {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final Storage storage;
    private final SecretKey secretKey;
    private final ObjectMapper mapper = new ObjectMapper();

    public StorageManager(Storage storage, SecretKey secretKey) {
        this.storage = storage;
        this.secretKey = secretKey;
    }

    public void init() {
        storage.init();
    }

    public int insertBot(BotDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.insertBot(dao);
        } catch (Throwable ex) {
            LOGGER.warn("insertBot: " + dto, ex);
            return -1;
        }
    }

    public boolean updateBot(BotDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.updateBot(dao);
        } catch (Throwable ex) {
            LOGGER.warn("updateBot: " + dto, ex);
            return false;
        }
    }

    public boolean deleteBot(BotDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.deleteBot(dao);
        } catch (Throwable ex) {
            LOGGER.warn("deleteBot: " + dto, ex);
            return false;
        }
    }

    public List<BotDto> queryBots() {
        final List<CommonDao> list = storage.queryBots();
        final List<BotDto> ret = new ArrayList<>();
        for (CommonDao dao : list) {
            try {
                ret.add(toDto(dao, BotDto.class));
            } catch (Throwable ex) {
                LOGGER.warn("queryBots: " + dao, ex);
            }
        }
        return ret;
    }

    public boolean insertChatId(String messageChatId, String messageId, String chatId) {
        try {
            return storage.insertChatId(messageKey(messageChatId, messageId),
                    CryptoUtils.encrypt(secretKey, chatId));
        } catch (Exception ex) {
            LOGGER.warn("insertChatId: " + messageChatId + ", " + messageId + ", " + chatId, ex);
            return false;
        }
    }

    public String getChatId(String messageChatId, String messageId) {
        try {
            return CryptoUtils.decrypt(secretKey, storage.getChatId(messageKey(messageChatId, messageId)));
        } catch (Exception ex) {
            LOGGER.warn("getChatId: " + messageChatId + ", " + messageId, ex);
            return null;
        }
    }

    public void deleteOldChatIds() {
        storage.deleteOldChatIds();
    }

    public int insertBan(BanDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.insertBan(dao);
        } catch (Throwable ex) {
            LOGGER.warn("insertBan: " + dto, ex);
            return -1;
        }
    }

    public boolean deleteBan(BanDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.deleteBan(dao);
        } catch (Throwable ex) {
            LOGGER.warn("deleteBot: " + dto, ex);
            return false;
        }
    }

    public List<BanDto> queryBans() {
        final List<CommonDao> list = storage.queryBans();
        final List<BanDto> ret = new ArrayList<>();
        for (CommonDao dao : list) {
            try {
                ret.add(toDto(dao, BanDto.class));
            } catch (Throwable ex) {
                LOGGER.warn("queryBots: " + dao, ex);
            }
        }
        return ret;
    }

    private <T extends CommonDto> CommonDao fromDto(T dto) throws Exception {
        final CommonDao dao = new CommonDao();
        dao.setId(dto.getId());
        dao.setBody(CryptoUtils.encrypt(secretKey, mapper.writeValueAsString(dto)));
        return dao;
    }

    private <T extends CommonDto> T toDto(CommonDao dao, Class<T> tClass) throws Exception {
        final T dto = mapper.readValue(CryptoUtils.decrypt(secretKey, dao.getBody()), tClass);
        dto.setId(dao.getId());
        return dto;
    }

    private String messageKey(String messageChatId, String messageId) throws Exception {
        return CryptoUtils.encrypt(secretKey, "" +
                "{ \"chatId\": \"" + messageChatId +
                "\", \"messageId\": \"" + messageId +
                "\"}"
        );
    }
}
