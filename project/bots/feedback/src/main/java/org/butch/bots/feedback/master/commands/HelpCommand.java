package org.butch.bots.feedback.master.commands;

import org.butch.bots.base.commands.BaseHelpCommand;
import org.butch.bots.feedback.master.FeedbackMasterBot;

public class HelpCommand extends BaseHelpCommand<FeedbackMasterBot> {
    public HelpCommand(FeedbackMasterBot bot) {
        super(bot);
    }

    @Override
    public String getHelp() {
        return "" +
                "<u><b>Описание работы бота.</b></u>\n" +
                "\n" +
                "Бот предназначен для управления другими ботами.\n" +
                "Боты под управлением данного получают следующие возможности:\n" +
                "  - Получать сообщения, написанные в бота, и отвечать на них.\n" +
                "\n" +
                "Информация о ботах хранится в базе данных в зашифрованом виде.\n" +
                "\n" +
                "Кроме этого, бот хранит в базе данных зашифрованные идентификаторы чатов, " +
                "через которые происходит общение. Это вызвано ограничением API телеграмма, " +
                "не позволяющего получить нужную информацию на лету. " +
                "Старые записи со временем удаляются.\n" +
                "\n" +
                "<u>Список поддерживаемых команд:</u>\n";
    }
}
