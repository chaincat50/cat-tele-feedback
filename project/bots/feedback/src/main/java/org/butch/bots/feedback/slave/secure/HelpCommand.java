package org.butch.bots.feedback.slave.secure;

import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.butch.bots.feedback.slave.commands.BaseHelpCommand;

public class HelpCommand extends BaseHelpCommand {
    public HelpCommand(BaseSlaveBot bot) {
        super(bot);
    }

    @Override
    public String getHelp() {
        return "" +
                "<u><b>Описание работы бота.</b></u>\n" +
                "\n" +
                "Данный бот предназначен для обратной связи. " +
                "Напишите в него то, что вы хотели бы передать.\n\n" +
                "Это безопасная версия бота. " +
                "Бот поддерживает пересылку только текста и изображений. " +
                "Длина пересылаемого текста не должна превышать " +
                Limits.MAX_TEXT_LENGTH + " символов. " +
                "Длина подписи к фото не должна превышать " +
                Limits.MAX_PHOTO_CAPTION_LENGTH + " символов. " +
                "Сообщения могут содержать вставки произвольных символов в конце - " +
                "не обращайте на них внимания.\n";
    }
}
