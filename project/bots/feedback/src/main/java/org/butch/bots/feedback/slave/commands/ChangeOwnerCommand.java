package org.butch.bots.feedback.slave.commands;

import org.butch.bots.base.Utils;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class ChangeOwnerCommand extends BaseOwnerCommand {
    public ChangeOwnerCommand(BaseSlaveBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "owner";
    }

    @Override
    public String getDescription() {
        return "Передать владение ботом";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " \n" +
                "Передаёт владение ботом другому пользователю. " +
                "Для этого, эта команда должа быть напечатана в ответе " +
                "на любое сообщение пользователя, которому передаётся владение.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        final Message reply = msg.getMessage().getReplyToMessage();
        if (reply == null) {
            bot.sendMessage(chatId, "" +
                    "Эта команда должна быть ответом на сообщение того, " +
                    "кому вы хотите передать владение ботом.");
            return;
        }
        final User futureOwner = reply.getFrom();
        final Result result = bot.changeOwner(userId, Long.toString(futureOwner.getId()));
        if (result == Result.OK) {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Новый владелец бота: " + Utils.getUsername(futureOwner) + "."
            );
        } else {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Не удалось сменить владельца. " + result.getDescription()
            );
        }
    }
}
